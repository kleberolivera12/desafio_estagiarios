data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "app-instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  key_name = "desafio-sust"
  subnet_id = aws_subnet.public1.id
  #
  vpc_security_group_ids = [aws_security_group.main_sec_gp.id]
  associate_public_ip_address = true

  tags = {
    Name = "app-instance"
  }
}

resource "aws_eip" "eip_ec2" {
  instance = aws_instance.app-instance.id
  vpc = true
  
  tags = {
    Name = "eip_app-instace"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.app-instance.id
  allocation_id = aws_eip.eip_ec2.id
}