resource "aws_vpc" "vpc_1" {
  cidr_block = "192.168.0.0/16"
  enable_dns_hostnames = true

    tags = {
      "Name" = "geral"
    }
}

# public subnets 

resource "aws_subnet" "public1" {
  vpc_id     = aws_vpc.vpc_1.id
  cidr_block = "192.168.0.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "geral-public1"
  }
}

resource "aws_subnet" "public2" {
  vpc_id     = aws_vpc.vpc_1.id
  cidr_block = "192.168.1.0/24"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "geral-public2"
  }
}

// private subnets 
resource "aws_subnet" "private1" {
  vpc_id     = aws_vpc.vpc_1.id
  cidr_block = "192.168.2.0/24" 
  availability_zone = "us-east-1a"

  tags = {
    Name = "geral-private1"
  }
}

resource "aws_subnet" "private2" {
  vpc_id     = aws_vpc.vpc_1.id
  cidr_block = "192.168.3.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "geral-private2"
  }
}

// database subnets 
resource "aws_subnet" "database-1" {
  vpc_id     = aws_vpc.vpc_1.id
  cidr_block = "192.168.4.0/24" 
  availability_zone = "us-east-1a"

  tags = {
    Name = "database-1"
  }
}

resource "aws_subnet" "database-2" {
  vpc_id     = aws_vpc.vpc_1.id
  cidr_block = "192.168.5.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "database-2"
  }
}



// internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc_1.id
}

// main route table
resource "aws_route_table" "route-tb-main" {
  vpc_id = aws_vpc.vpc_1.id

  tags = {
    Name = "route-tb-main"
  }
}
resource "aws_main_route_table_association" "route-main" {
  vpc_id         = aws_vpc.vpc_1.id
  route_table_id = aws_route_table.route-tb-main.id
}

// public route table
resource "aws_route_table" "route-public" {
  vpc_id = aws_vpc.vpc_1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name = "route-public"
  }
}

// private route table
resource "aws_eip" "elastic-nat" {
    vpc = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.elastic-nat.id
  subnet_id     = aws_subnet.public1.id

  tags = {
    Name = "nat-gw"
  }

  depends_on = [aws_internet_gateway.gw]
}

resource "aws_route_table" "route-private" {
  vpc_id = aws_vpc.vpc_1.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat.id
  }
  tags = {
    Name = "route-private"
  }
}

# private route table
resource "aws_route_table" "route-db-private" {
  vpc_id = aws_vpc.vpc_1.id
  tags = {
    Name = "route-db-private"
  }
}


# subnet association
resource "aws_route_table_association" "associate-public1" {
  subnet_id      = aws_subnet.public1.id
  route_table_id = aws_route_table.route-public.id
}
resource "aws_route_table_association" "associate-public2" {
  subnet_id      = aws_subnet.public2.id
  route_table_id = aws_route_table.route-public.id
}
resource "aws_route_table_association" "associate-private1" {
  subnet_id      = aws_subnet.private1.id
  route_table_id = aws_route_table.route-private.id
}
resource "aws_route_table_association" "associate-private2" {
  subnet_id      = aws_subnet.private2.id
  route_table_id = aws_route_table.route-private.id
}
resource "aws_route_table_association" "associate-db-1" {
  subnet_id      = aws_subnet.database-1.id
  route_table_id = aws_route_table.route-db-private.id
}
resource "aws_route_table_association" "associate-db-2" {
  subnet_id      = aws_subnet.database-2.id
  route_table_id = aws_route_table.route-db-private.id
}

resource "aws_security_group" "main_sec_gp" {
    vpc_id = aws_vpc.vpc_1.id
    name = "main_sec_gp"
    description = "sec-gp-desafio"

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    // allow ssh
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]

    }
    // allow http 
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "prod_security_group"
    }
}