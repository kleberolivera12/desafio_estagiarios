// domain name variable
variable "domain_name" {
  default = "static.klb2022.cf"
}

// root domain
variable "root_domain_name" {
  default = "klb2022.cf"
}