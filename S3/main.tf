resource "aws_s3_bucket" "bucket_desafio" {
  bucket = "staticwebsite-klb-desafio"
#  acl = "public-read"
}

locals {
  s3_origin_id = aws_s3_bucket.bucket_desafio.id
}

resource "aws_s3_bucket_acl" "acl_bucket" {
  bucket = aws_s3_bucket.bucket_desafio.id
  acl    = "public-read"
}

# configurando policy
resource "aws_s3_bucket_policy" "policy_website" {
  bucket = aws_s3_bucket.bucket_desafio.id 
  policy = file("policy.json")
}


resource "aws_s3_bucket_website_configuration" "websiteconfig" {
  bucket = aws_s3_bucket.bucket_desafio.id
  index_document {
    suffix = "index.html"
  }
  error_document {
    key = "error.html"
  }
}

# criando pagina principal index
resource "aws_s3_object" "index" {
  bucket = aws_s3_bucket.bucket_desafio.bucket
  key    = "index.html"
  source = "index.html"
  acl    = "public-read"
  content_type = "text/html"

  etag = filemd5("index.html")
}

# criando pagina de erro
resource "aws_s3_object" "arq-error" {
  bucket = aws_s3_bucket.bucket_desafio.bucket
  key    = "error.html"
  source = "error.html"
  acl    = "public-read"
  content_type = "text/html"

  etag = filemd5("error.html")
}

// Create an ACM Certificate
resource "aws_acm_certificate" "certificate" {
  domain_name       = "*.${var.root_domain_name}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "primary" {
  name         = "klb2022.cf"
  private_zone = false
}

// add DNS record
resource "aws_route53_record" "cert_dns" {
  name =  tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_name
  records = [tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_value]
  type = tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_type

  allow_overwrite = true
  zone_id = data.aws_route53_zone.primary.zone_id
  ttl = 60
}

// validate the certificate
resource "aws_acm_certificate_validation" "cert_validate" {
  certificate_arn = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [aws_route53_record.cert_dns.fqdn]
}

# acm
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {}

resource "aws_cloudfront_distribution" "static_distribution" {
  origin {
    domain_name = aws_s3_bucket.bucket_desafio.bucket_domain_name
    origin_id = aws_s3_bucket.bucket_desafio.bucket
  }
  
  # By default, show index.html file
  default_root_object = "index.html"
  enabled = true
  //is_ipv6_enabled = true
  aliases = [var.domain_name]

  # If there is a 404, return index.html with a HTTP 200 Response
  custom_error_response {
    error_caching_min_ttl = 3000
    error_code = 404
    response_code = 200
    response_page_path = "/index.html"
  }
  
  default_cache_behavior {
    allowed_methods = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.bucket_desafio.bucket
    
    # Forward all query strings, cookies and headers
    forwarded_values {
      query_string = true
       cookies {
          forward = "none"
       }
    }
    viewer_protocol_policy = "redirect-to-https"
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

   // Here's where our certificate is loaded in!
  viewer_certificate {
    acm_certificate_arn = "${aws_acm_certificate.certificate.arn}"
    ssl_support_method  = "sni-only"
  }
}

// adcionando dominio para CloudFront
data "aws_route53_zone" "zone" {
  name         = var.root_domain_name
  private_zone = false
}

resource "aws_route53_record" "CloudFront_record" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = var.domain_name
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.static_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.static_distribution.hosted_zone_id
    evaluate_target_health = true
  }
}