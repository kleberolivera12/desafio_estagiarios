provider "aws" {
  region = "us-east-1"
  profile = "default"

  default_tags {
    tags = {
        "owner" = "kleber-sysop"
        "environment" = "test"
        "project" = "desafio_estagiarios" 
    }
  }
}
